// Google Sheer ID - 1TsC2me9i_kwPFpwMnA1aEtOqK3lR-uSMN2LSF-_iZNU

const apiKey        = "AIzaSyCH__svzz7en7dxLclZKkhHAedMae12k30",
      spreadsheetID = "1TsC2me9i_kwPFpwMnA1aEtOqK3lR-uSMN2LSF-_iZNU",
      dataRange     = "A2:N",
      filterTotal   = 14,
      debugging     = false;

var sheetData,

    countryOptionArray            = [],
    siteOptionArray               = [],
    publisherOptionArray          = [],
    primaryContextOptionArray     = [],
    genderOptionArray             = [],
    mastheadOptionArray           = [],
    dmpuOptionArray               = [],
    riseOptionArray               = [],
    skinsOptionArray              = [],
    miniInterscrollerOptionArray  = [],

    tempArray = [],
    queryArray = ["", "", "", "", "", "", "", "", "", ""],

    xlsx,

    i, j, k, dropdown, button, type;





function init() {
  console.log(":: page ready ::");

  if (debugging) {
    overlay.style.display = "none";
  } else {
    gapi.load('client', loadSheet);
  }
}


function loadSheet() {
  gapi.load('client', {
    callback: function() {
    // Handle gapi.client initialization.
    // initGapiClient();
    },
    onerror: function() {
    // Handle loading error.
    alert('gapi.client failed to load!');
    },
    timeout: 5000, // 5 seconds.
    ontimeout: function() {
      // Handle timeout.
      alert('gapi.client could not load in a timely manner!');
    }
  });

  gapi.client.setApiKey(apiKey);

  return gapi.client.load("https://content.googleapis.com/discovery/v1/apis/sheets/v4/rest")
                    .then(function() {
                            console.log("GAPI client loaded for API");

                            return gapi.client.sheets.spreadsheets.values.get({
                              "spreadsheetId": spreadsheetID,
                              "range": dataRange
                            })
                                .then(function(response) {
                                        // Handle the results here (response.result has the parsed body).
                                        //console.log("Response", response);
                                        sheetData = response.result.values;
                                        manipulateData();
                                      },
                                      function(err) {
                                        console.error("Execute error", err);
                                      });
                          },
                          function(err) {
                            console.error("Error loading GAPI client for API", err);
                          });


  console.log(":: load sheet ::");
}

function manipulateData() {
  console.log(":: manipulating data ::");
  // Go through Sheet to get all necessary values and remove any duplicates

  // A - 0 - Country
  // B - 1 - Site
  // C - 2 - Publisher
  // E - 4 - Primary Context
  // G - 6 - Gender
  // I - 8 - Masthead
  // J - 9 - DMPU
  // L - 11 - Rise
  // M - 12 - Skins
  // N - 13 - Mini/Interscroller

  //Remove any empty rows
  i = sheetData.length;
  while (i--) {
    if (sheetData[i].length < 1 || sheetData[i] == undefined) sheetData.splice(i, 1);
  }

  //Change blank elements within a row to '-'
  for (i = 0; i < sheetData.length; i++) {
    for (j = 1; j < filterTotal; j++) {
      switch (sheetData[i][j]) {
        case "":
        case " ":
        case undefined:
        case null:
          sheetData[i][j] = "-";
          break;
      }

      //Specifically for sites, publishers and mastheads
      switch (j) {
        case 2:
          sheetData[i][j] = titleCase(fixString(String(sheetData[i][j])));
          break;
        case 1:
        case 4:
        case 11:
          sheetData[i][j] = fixString(String(sheetData[i][j]));
          // console.log(sheetData[i][j]);
          break;
      }
    }
  }


  //Set option arrays
  for (i = 0; i < sheetData.length; i++) {
    countryOptionArray[i] = sheetData[i][0].trim();
    siteOptionArray[i] = sheetData[i][1];
    publisherOptionArray[i] = titleCase(sheetData[i][2]);
    primaryContextOptionArray[i] = sheetData[i][4];
    genderOptionArray[i] = sheetData[i][6];
    mastheadOptionArray[i] = sheetData[i][8];
    dmpuOptionArray[i] = sheetData[i][9];
    riseOptionArray[i] = sheetData[i][11];
    skinsOptionArray[i] = sheetData[i][12];
    miniInterscrollerOptionArray[i] = sheetData[i][13];

    // console.log(typeof sheetData[i][4]);
  }


  //Remove duplicates
  countryOptionArray = countryOptionArray.filter(onlyUnique);
  siteOptionArray = siteOptionArray.filter(onlyUnique);
  publisherOptionArray = publisherOptionArray.filter(onlyUnique);
  primaryContextOptionArray = primaryContextOptionArray.filter(onlyUnique);
  genderOptionArray = genderOptionArray.filter(onlyUnique);
  mastheadOptionArray = mastheadOptionArray.filter(onlyUnique);
  dmpuOptionArray = dmpuOptionArray.filter(onlyUnique);
  riseOptionArray = riseOptionArray.filter(onlyUnique);
  skinsOptionArray = skinsOptionArray.filter(onlyUnique);
  miniInterscrollerOptionArray = miniInterscrollerOptionArray.filter(onlyUnique);


  //Sort alphabetically
  countryOptionArray.sort();
  siteOptionArray.sort();
  publisherOptionArray.sort();
  primaryContextOptionArray.sort();
  genderOptionArray.sort();
  mastheadOptionArray.sort();
  dmpuOptionArray.sort();
  riseOptionArray.sort();
  skinsOptionArray.sort();
  miniInterscrollerOptionArray.sort();


  // console.log(countryOptionArray);
  // console.log(siteOptionArray);
  // console.log(publisherOptionArray);
  // console.log(primaryContextOptionArray);
  // console.log(genderOptionArray);
  // console.log(mastheadOptionArray);
  // console.log(dmpuOptionArray);
  // console.log(riseOptionArray);
  // console.log(skinsOptionArray);
  // console.log(miniInterscrollerOptionArray);



  console.log(":: initial loaded data ::", sheetData);


  setDropdowns();
}

function setDropdowns() {
  console.log(":: setting dropdown menus ::");

  var dropDownArray = [
        document.getElementById("countryDropdown"), document.getElementById("siteDropdown"), document.getElementById("publisherDropdown"),
        document.getElementById("primaryContextDropdown"), document.getElementById("genderDropdown"), document.getElementById("mastheadDropdown"),
        document.getElementById("dmpuDropdown"), document.getElementById("riseDropdown"), document.getElementById("skinsDropdown"),
        document.getElementById("miniInterscrollerDropdown")
      ],
      optionArray = [
        countryOptionArray, siteOptionArray, publisherOptionArray, primaryContextOptionArray, genderOptionArray,
        mastheadOptionArray, dmpuOptionArray, riseOptionArray, skinsOptionArray, miniInterscrollerOptionArray
      ],
      selectList, optionList;

  for (i = 0; i < dropDownArray.length; i++) {
    selectList = dropDownArray[i];
    optionList = optionArray[i];

    var option = document.createElement("option");
    option.setAttribute("value", "");
    option.text = "All";
    selectList.appendChild(option);
  }

  for (i = 0; i < dropDownArray.length; i++) {
    selectList = dropDownArray[i];
    optionList = optionArray[i];
    for (j = 0; j < optionList.length; j++) {
      var option = document.createElement("option");
      option.setAttribute("value", optionList[j]);
      option.text = optionList[j];
      selectList.appendChild(option);
    }
  }

  for (i = 0; i < dropDownArray.length; i++) dropDownArray[i].addEventListener("change", dropDownHandler);

  document.getElementById("resetButton").addEventListener("click", buttonHandler);
  document.getElementById("downloadButton").addEventListener("click", buttonHandler);
  document.getElementById("viewButton").addEventListener("click", buttonHandler);

  tempArray = sheetData.slice(0);
  console.log(":: new data array created ::");

  setTimeout(function() { overlay.style.display = "none"; }, 1000);
}

function updateDropdowns($index) {
  // document.getElementById("countryDropdown").options[10].style.display = "none";
  // document.getElementById("countryDropdown").options[10].style.visibility = "hidden";
  // console.log("ASDASD", document.getElementById("countryDropdown").options[10]);

  // dropdown.options[dropdown.selectedIndex].value;



  // A - 0 - Country
  // B - 1 - Site
  // C - 2 - Publisher
  // E - 4 - Primary Context
  // G - 6 - Gender
  // I - 8 - Masthead
  // J - 9 - DMPU
  // L - 11 - Rise
  // M - 12 - Skins
  // N - 13 - Mini/Interscroller

  //Need to update all dropdowns apart from the one that was last changed to update the dropdowns


  var dropDownArray = [
        document.getElementById("countryDropdown"), document.getElementById("siteDropdown"), document.getElementById("publisherDropdown"),
        document.getElementById("primaryContextDropdown"), document.getElementById("genderDropdown"), document.getElementById("mastheadDropdown"),
        document.getElementById("dmpuDropdown"), document.getElementById("riseDropdown"), document.getElementById("skinsDropdown"),
        document.getElementById("miniInterscrollerDropdown")
      ];

  for (i = 0; i < dropDownArray.length; i++) {
    // console.log("dropdown", dropDownArray[i].options);
    var options = dropDownArray[i].options;

    if (i != $index) {

      for (j = 0; j < options.length; j++) {
        // console.log("option", options[j]);

        for (k = 0; k < tempArray.length; k++) {
          // console.log(i);
          // if (options[j] === tempArray[k]) console.log("ASDASDASDASD");
          // if (options[j] != "All" && options[j] != tempArray[k]) {
          //   options[j].style.display = "none";
          // }

          // console.log(i, tempArray[k]);
        }

      }

    }
  }

  // for (i = 0; i < dropDownArray[0].options.length; i++) {
  //   for (j = 0; j < tempArray.length; j++) {
  //     if (dropDownArray[0].options[i] != tempArray[j][0]) {
  //       if ($index != 0) dropDownArray[0].options[i].style.display = "none";
  //       // console.log("removed", dropDownArray[0].options[i]);
  //     } else {
  //       dropDownArray[0].options[i].style.display = "block";
  //     }
  //   }
  // }
  //
  // for (i = 0; i < dropDownArray[1].options.length; i++) {
  //   for (j = 0; j < tempArray.length; j++) {
  //     if (dropDownArray[1].options[i] != tempArray[j][1] && dropDownArray[1].options[i] != "All") {
  //       if ($index != 1) dropDownArray[1].options[i].style.display = "none";
  //     } else {
  //       dropDownArray[1].options[i].style.display = "block";
  //     }
  //   }
  // }
}

function filterData($queryIndex, $query) {
  console.log($queryIndex, $query);
  console.log(":: filtering ::", tempArray);

  i = tempArray.length;

  while (i--) {
    if (tempArray[i][$queryIndex] === $query) console.log(i, tempArray[i][$queryIndex]);
    if (tempArray[i][$queryIndex] != $query) tempArray.splice(i, 1);
  }

  console.log(":: filtered results - " + $query + " ::", tempArray);

  (tempArray.length === 1) ? document.getElementById("resultsText").innerHTML = tempArray.length + " site found" : document.getElementById("resultsText").innerHTML = tempArray.length + " sites found";
}

function autoFilterData() {
  tempArray = sheetData.slice(0);

  var q1 = tempArray.length, q2 = tempArray.length, q3 = tempArray.length, q4 = tempArray.length, q5 = tempArray.length,
      q6 = tempArray.length, q7 = tempArray.length, q8 = tempArray.length, q9 = tempArray.length, q10 = tempArray.length;

  // A - 0 - Country
  // B - 1 - Site
  // C - 2 - Publisher
  // E - 4 - Primary Context
  // G - 6 - Gender
  // I - 8 - Masthead
  // J - 9 - DMPU
  // L - 11 - Rise
  // M - 12 - Skins
  // N - 13 - Mini/Interscroller

  // console.log(queryArray);

  for (i = 0; i < queryArray.length; i++) {
    var q = tempArray.length;
    switch (i) {
      case 0:
        while (q--) { if (queryArray[i] != "") { if (tempArray[q][0] != queryArray[i]) tempArray.splice(q, 1); } }
        break;
      case 1:
        while (q--) { if (queryArray[i] != "") { if (tempArray[q][1] != queryArray[i]) tempArray.splice(q, 1); } }
        break;
      case 2:
        while (q--) { if (queryArray[i] != "") { if (tempArray[q][2] != queryArray[i]) tempArray.splice(q, 1); } }
        break;
      case 3:
        while (q--) { if (queryArray[i] != "") { if (tempArray[q][4] != queryArray[i]) tempArray.splice(q, 1); } }
        break;
      case 4:
        while (q--) { if (queryArray[i] != "") { if (tempArray[q][6] != queryArray[i]) tempArray.splice(q, 1); } }
        break;
      case 5:
        while (q--) { if (queryArray[i] != "") { if (tempArray[q][8] != queryArray[i]) tempArray.splice(q, 1); } }
        break;
      case 6:
        while (q--) { if (queryArray[i] != "") { if (tempArray[q][9] != queryArray[i]) tempArray.splice(q, 1); } }
        break;
      case 7:
        while (q--) { if (queryArray[i] != "") { if (tempArray[q][11] != queryArray[i]) tempArray.splice(q, 1); } }
        break;
      case 8:
        while (q--) { if (queryArray[i] != "") { if (tempArray[q][12] != queryArray[i]) tempArray.splice(q, 1); } }
        break;
      case 9:
        while (q--) { if (queryArray[i] != "") { if (tempArray[q][13] != queryArray[i]) tempArray.splice(q, 1); } }
        break;
    }
  }

  console.log(":: filtered results - ", tempArray);

  (tempArray.length === 1) ? document.getElementById("resultsText").innerHTML = tempArray.length + " site found" : document.getElementById("resultsText").innerHTML = tempArray.length + " sites found";


  // updateDropdowns();
    // document.getElementById("countryDropdown").options[10].style.display = "none";
    // document.getElementById("countryDropdown").options[10].style.visibility = "hidden";
    // console.log("ASDASD", document.getElementById("countryDropdown").options[10]);

    // dropdown.options[dropdown.selectedIndex].value;
}

function saveData() {
  // var a = [
  //           [{value:'Income - Webshop', type:'string'}, {value: "asd", type:'string'}],
  //           [{value:'Income - Webshop2', type:'string'}, {value: "asd2", type:'string'}]
  //         ];

  // A - 0 - Country
  // B - 1 - Site
  // C - 2 - Publisher
  // E - 4 - Primary Context
  // G - 6 - Gender
  // I - 8 - Masthead
  // J - 9 - DMPU
  // L - 11 - Rise
  // M - 12 - Skins
  // N - 13 - Mini/Interscroller

  var a = [], b, c, d;

  for (i = 0; i < tempArray.length; i++) {
    var t = [];
    t.push({value:tempArray[i][0], type:"string"});
    t.push({value:tempArray[i][1], type:"string"});
    t.push({value:tempArray[i][2], type:"string"});
    t.push({value:tempArray[i][4], type:"string"});
    t.push({value:tempArray[i][6], type:"string"});
    t.push({value:tempArray[i][8], type:"string"});
    t.push({value:tempArray[i][9], type:"string"});
    t.push({value:tempArray[i][11], type:"string"});
    t.push({value:tempArray[i][12], type:"string"});
    t.push({value:tempArray[i][13], type:"string"});
    a[i] = t;
  }

  b = [
    {value:"Country", type:"string"},
    {value:"Site", type:"string"},
    {value:"Publisher", type:"string"},
    {value:"Primary Context", type:"string"},
    {value:"Gender", type:"string"},
    {value:"Masthead", type:"string"},
    {value:"DMPU", type:"string"},
    {value:"Rise", type:"string"},
    {value:"Skins", type:"string"},
    {value:"Mini/Interscroller", type:"string"}
  ];
  a.unshift(b);

  console.log(a);

  d = new Date();
  // c = "custom-site-list-" + d.getYear() + (d.getMonth() + 1) + d.getUTCDate() + "_" + d.getHours() + d.getMinutes() + d.getSeconds();
  c = "custom-site-list-" + new Date().getTime();

  // console.log(new Date().getTime());

  xlsx = {
    filename: c,
    sheet: {
	     data: a
    }
  }

  zipcelx(xlsx);
}

function openData() {
  var w = window.open("", "Basic Custom Site List", "width=400,height=400,resizeable,scrollbars"), content = "<p>", temp = [];

  for (i = 0; i < tempArray.length; i++) temp[i] = tempArray[i][1];
  temp.sort();

  // for (i = 0; i < tempArray.length; i++) content += tempArray[i][1] + "<br/>";
  for (i = 0; i < tempArray.length; i++) content += temp[i] + "<br/>";
  content += "</p>";
  w.document.title = "TI Media Publishing - Basic Custom Site List";
  w.document.body.innerHTML = content;
}

function resetDropDowns() {
  var dropDownArray = [
        document.getElementById("countryDropdown"), document.getElementById("siteDropdown"), document.getElementById("publisherDropdown"),
        document.getElementById("primaryContextDropdown"), document.getElementById("genderDropdown"), document.getElementById("mastheadDropdown"),
        document.getElementById("dmpuDropdown"), document.getElementById("riseDropdown"), document.getElementById("skinsDropdown"),
        document.getElementById("miniInterscrollerDropdown")
      ];

  for (i = 0; i < dropDownArray.length; i++) dropDownArray[i].selectedIndex = 0;

  tempArray = [];
  tempArray = sheetData.slice(0);
  console.log(":: new data array created ::");
  console.log(":: tempArray ::", tempArray);

  queryArray = ["", "", "", "", "", "", "", "", "", ""];
}

function dropDownHandler($e) {
  dropdown = $e.currentTarget;
  type = $e.type;

  resultsCounter.style.display = "block";
  // buttons.style.display = "block";

  // A - 0 - Country
  // B - 1 - Site
  // C - 2 - Publisher
  // E - 4 - Primary Context
  // G - 6 - Gender
  // I - 8 - Masthead
  // J - 9 - DMPU
  // L - 11 - Rise
  // M - 12 - Skins
  // N - 13 - Mini/Interscroller

  switch (dropdown) {
    case document.getElementById("countryDropdown"):
      queryArray[0] = dropdown.options[dropdown.selectedIndex].value;
      updateDropdowns(0);
      // filterData(0, dropdown.options[dropdown.selectedIndex].value);
      break;

    case document.getElementById("siteDropdown"):
      queryArray[1] = dropdown.options[dropdown.selectedIndex].value;
      // updateDropdowns(1);
      // filterData(1, dropdown.options[dropdown.selectedIndex].value);
      break;

    case document.getElementById("publisherDropdown"):
      queryArray[2] = dropdown.options[dropdown.selectedIndex].value;
      // updateDropdowns(2);
      // filterData(2, dropdown.options[dropdown.selectedIndex].value);
      break;

    case document.getElementById("primaryContextDropdown"):
      queryArray[3] = dropdown.options[dropdown.selectedIndex].value;
      // updateDropdowns(3);
      // filterData(4, dropdown.options[dropdown.selectedIndex].value);
      break;

    case document.getElementById("genderDropdown"):
      queryArray[4] = dropdown.options[dropdown.selectedIndex].value;
      // updateDropdowns(4);
      // filterData(6, dropdown.options[dropdown.selectedIndex].value);
      break;

    case document.getElementById("mastheadDropdown"):
      queryArray[5] = dropdown.options[dropdown.selectedIndex].value;
      // updateDropdowns(5);
      // filterData(8, dropdown.options[dropdown.selectedIndex].value);
      break;

    case document.getElementById("dmpuDropdown"):
      queryArray[6] = dropdown.options[dropdown.selectedIndex].value;
      // updateDropdowns(6);
      // filterData(9, dropdown.options[dropdown.selectedIndex].value);
      break;

    case document.getElementById("riseDropdown"):
      queryArray[7] = dropdown.options[dropdown.selectedIndex].value;
      // updateDropdowns(7);
      // filterData(11, dropdown.options[dropdown.selectedIndex].value);
      break;

    case document.getElementById("skinsDropdown"):
      queryArray[8] = dropdown.options[dropdown.selectedIndex].value;
      // updateDropdowns(8);
      // filterData(12, dropdown.options[dropdown.selectedIndex].value);
      break;

    case document.getElementById("miniInterscrollerDropdown"):
      queryArray[9] = dropdown.options[dropdown.selectedIndex].value;
      // updateDropdowns(9);
      // filterData(13, dropdown.options[dropdown.selectedIndex].value);
      break;
  }

  autoFilterData();
}

function buttonHandler($e) {
  button = $e.currentTarget;
  type = $e.type;

  switch (button) {
    case document.getElementById("resetButton"):
      console.log(":: cleared and reset data ::");

      resultsCounter.style.display = "none";
      // buttons.style.display = "none";
      resetDropDowns();
      break;

    case document.getElementById("downloadButton"):
      saveData();
      break;

    case document.getElementById("viewButton"):
      openData();
      break;
  }
}

function onlyUnique($value, $index, $self) { return $self.indexOf($value) === $index; }

function fixString($string) {
  if ($string.substring($string.length - 1, $string.length) === "/") $string = $string.substring(0, $string.length - 1);
  $string = $string.toLowerCase();
  $string = $string.trim();
  return $string.charAt(0).toUpperCase() + $string.slice(1);
}

function titleCase($string) {
  return $string.replace(/\w\S*/g, function($txt) {
    return $txt.charAt(0).toUpperCase() + $txt.substr(1).toLowerCase();
  });
}

domready(function() {
	console.log(":: dom ready ::");
  if (divScraper) init();
});
