const apiKey                      = "AIzaSyCH__svzz7en7dxLclZKkhHAedMae12k30",
      spreadsheetID               = "1TsC2me9i_kwPFpwMnA1aEtOqK3lR-uSMN2LSF-_iZNU",
      dataRange                   = "A2:T",
      filterTotal                 = 26,//14,
      debugging                   = false;

var sheetData,
    countryOptionArray            = [],
    siteOptionArray               = [],
    publisherOptionArray          = [],
    primaryContextOptionArray     = [],
    genderOptionArray             = [],
    mastheadOptionArray           = [],
    dmpuOptionArray               = [],
    riseOptionArray               = [],
    skinsOptionArray              = [],
    miniInterscrollerOptionArray  = [],

    tempArray                     = [],
    customArray                   = [],
    queryArray                    = ["", "", "", "", "", "", "", "", "", ""],

    xlsx,

    i, j, k, dropdown, button, type;





function init() {
  console.log(":: page ready ::");

  Tipped.create('.tooltipLeft', {position:"left"});
  Tipped.create('.tooltipRight', {position:"right"});
  Tipped.create('.tooltipBottom', {position:"bottom"});

  (debugging) ? overlay.style.display = "none" : gapi.load('client', loadSheet);
}





function loadSheet() {
  console.log(":: load google sheet ::");

  gapi.load('client', {
    callback: function() {
      // Handle gapi.client initialization.
      // initGapiClient();
    },
    onerror: function() {
      // Handle loading error.
      console.log('gapi.client failed to load!');
    },
    timeout: 5000, // 5 seconds.
    ontimeout: function() {
      // Handle timeout.
      console.log('gapi.client could not load in a timely manner!');
    }
  });

  gapi.client.setApiKey(apiKey);

  return gapi.client.load("https://content.googleapis.com/discovery/v1/apis/sheets/v4/rest")
                    .then(function() {
                            console.log("GAPI client loaded for API");

                            return gapi.client.sheets.spreadsheets.values.get({
                              "spreadsheetId": spreadsheetID,
                              "range": dataRange
                            })
                                .then(function(response) {
                                        // Handle the results here (response.result has the parsed body).
                                        console.log("Response", response);
                                        sheetData = response.result.values;
                                        tempArray = sheetData.slice(0);
                                        manipulateData();
                                      },
                                      function(err) {
                                        console.error("Execute error", err);
                                      });
                          },
                          function(err) {
                            console.error("Error loading GAPI client for API", err);
                          });
}





function manipulateData() {
  console.log(":: manipulating data ::");

  // A - 0 - Country
  // B - 1 - Site
  // C - 2 - Publisher
  // E - 4 - Primary Context
  // G - 6 - Gender
  // I - 8 - Masthead
  // J - 9 - DMPU
  // L - 11 - Rise
  // M - 12 - Skins
  // N - 13 - Mini/Interscroller

  //Remove any empty rows
  i = sheetData.length;
  while (i--) { if (sheetData[i].length < 1 || sheetData[i] == undefined) sheetData.splice(i, 1); }

  //Change blank elements within a row to '-'
  for (i = 0; i < sheetData.length; i++) {
    for (j = 1; j < filterTotal; j++) {
      switch (sheetData[i][j]) {
        case "":
        case " ":
        case undefined:
        case null:
          sheetData[i][j] = "-";
          break;
      }

      //Specifically for sites, publishers and mastheads
      switch (j) {
        case 2:
          sheetData[i][j] = titleCase(fixString(String(sheetData[i][j])));
          break;
        case 1:
        case 4:
        case 11:
          sheetData[i][j] = fixString(String(sheetData[i][j]));
          // console.log(sheetData[i][j]);
          break;
      }
    }
  }

  i = sheetData.length;
  while (i--) { if (sheetData[i][1] === "-") sheetData.splice(i, 1); }


  //Set option arrays
  for (i = 0; i < sheetData.length; i++) {
    countryOptionArray[i]             = sheetData[i][0].trim();
    siteOptionArray[i]                = sheetData[i][1];
    publisherOptionArray[i]           = titleCase(sheetData[i][2]);
    primaryContextOptionArray[i]      = sheetData[i][4];
    genderOptionArray[i]              = sheetData[i][6];
    mastheadOptionArray[i]            = sheetData[i][8];
    dmpuOptionArray[i]                = sheetData[i][9];
    riseOptionArray[i]                = sheetData[i][11];
    skinsOptionArray[i]               = sheetData[i][12];
    miniInterscrollerOptionArray[i]   = sheetData[i][13];

    // console.log(typeof sheetData[i][4]);
  }


  //Remove duplicates
  countryOptionArray                  = countryOptionArray.filter(onlyUnique);
  siteOptionArray                     = siteOptionArray.filter(onlyUnique);
  publisherOptionArray                = publisherOptionArray.filter(onlyUnique);
  primaryContextOptionArray           = primaryContextOptionArray.filter(onlyUnique);
  genderOptionArray                   = genderOptionArray.filter(onlyUnique);
  mastheadOptionArray                 = mastheadOptionArray.filter(onlyUnique);
  dmpuOptionArray                     = dmpuOptionArray.filter(onlyUnique);
  riseOptionArray                     = riseOptionArray.filter(onlyUnique);
  skinsOptionArray                    = skinsOptionArray.filter(onlyUnique);
  miniInterscrollerOptionArray        = miniInterscrollerOptionArray.filter(onlyUnique);


  //Sort alphabetically
  countryOptionArray.sort();
  siteOptionArray.sort();
  publisherOptionArray.sort();
  primaryContextOptionArray.sort();
  genderOptionArray.sort();
  mastheadOptionArray.sort();
  dmpuOptionArray.sort();
  riseOptionArray.sort();
  skinsOptionArray.sort();
  miniInterscrollerOptionArray.sort();


  // console.log(countryOptionArray);
  // console.log(siteOptionArray);
  // console.log(publisherOptionArray);
  // console.log(primaryContextOptionArray);
  // console.log(genderOptionArray);
  // console.log(mastheadOptionArray);
  // console.log(dmpuOptionArray);
  // console.log(riseOptionArray);
  // console.log(skinsOptionArray);
  // console.log(miniInterscrollerOptionArray);


  console.log(":: google sheet loaded ::", sheetData);

  setDropdowns();
}





function setDropdowns() {
  console.log(":: setting dropdown menus ::");

  var dropDownArray = [
    document.getElementById("countryDropdown"), document.getElementById("siteDropdown"), document.getElementById("publisherDropdown"),
    document.getElementById("primaryContextDropdown"), document.getElementById("genderDropdown"), document.getElementById("mastheadDropdown"),
    document.getElementById("dmpuDropdown"), document.getElementById("riseDropdown"), document.getElementById("skinsDropdown"),
    document.getElementById("miniInterscrollerDropdown")
  ],
  optionArray = [
    countryOptionArray, siteOptionArray, publisherOptionArray, primaryContextOptionArray, genderOptionArray,
    mastheadOptionArray, dmpuOptionArray, riseOptionArray, skinsOptionArray, miniInterscrollerOptionArray
  ],
  selectList, optionList;

  for (i = 0; i < dropDownArray.length; i++) {
    selectList = dropDownArray[i];
    optionList = optionArray[i];

    var option = document.createElement("option");
    option.setAttribute("value", "");
    option.text = "All";
    selectList.appendChild(option);
  }

  for (i = 0; i < dropDownArray.length; i++) {
    selectList = dropDownArray[i];
    optionList = optionArray[i];
    for (j = 0; j < optionList.length; j++) {
      var option = document.createElement("option");

      // if (optionList[j] === "TR") {
      //   option.setAttribute("value", optionList[j] + "-fix");
      //   option.text = optionList[j];
      // } else {
        option.setAttribute("value", optionList[j]);
        option.text = optionList[j];
      // }
      selectList.appendChild(option);
    }
  }

  for (i = 0; i < dropDownArray.length; i++) dropDownArray[i].addEventListener("change", dropDownHandler);

  document.getElementById("addButton").addEventListener("click", buttonHandler);
  document.getElementById("clearButton").addEventListener("click", buttonHandler);
  document.getElementById("selectButton").addEventListener("click", buttonHandler);
  document.getElementById("copyButton").addEventListener("click", buttonHandler);
  document.getElementById("viewButton").addEventListener("click", buttonHandler);
  document.getElementById("downloadButton").addEventListener("click", buttonHandler);
  document.getElementById("resetButton").addEventListener("click", buttonHandler);

  tempArray = sheetData.slice(0);
  console.log(":: new data array created ::");

  setTimeout(function() {
    overlay.style.display = "none";
    preview.style.display = "block";
    output.style.display = "block";
  }, 1000);
}

function updateDropdowns($reset, $index) {
  // A - 0 - Country
  // B - 1 - Site
  // C - 2 - Publisher
  // E - 4 - Primary Context
  // G - 6 - Gender
  // I - 8 - Masthead
  // J - 9 - DMPU
  // L - 11 - Rise
  // M - 12 - Skins
  // N - 13 - Mini/Interscroller

  console.log(":: updating dropdown menus ::");

  var dropDownArray = [
    document.getElementById("countryDropdown"), document.getElementById("siteDropdown"), document.getElementById("publisherDropdown"),
    document.getElementById("primaryContextDropdown"), document.getElementById("genderDropdown"), document.getElementById("mastheadDropdown"),
    document.getElementById("dmpuDropdown"), document.getElementById("riseDropdown"), document.getElementById("skinsDropdown"),
    document.getElementById("miniInterscrollerDropdown")
  ],
  optionArray = [
    [], [], [], [], [], [], [], [], [], []
  ],
  selectList, optionList;


  for (i = 0; i < tempArray.length; i++) {
    optionArray[0].push(tempArray[i][0].trim());
    optionArray[1].push(tempArray[i][1]);
    optionArray[2].push(tempArray[i][2]);
    optionArray[3].push(tempArray[i][4]);
    optionArray[4].push(tempArray[i][6]);
    optionArray[5].push(tempArray[i][8]);
    optionArray[6].push(tempArray[i][9]);
    optionArray[7].push(tempArray[i][11]);
    optionArray[8].push(tempArray[i][12]);
    optionArray[9].push(tempArray[i][13]);
  }

  for (i = 0; i < optionArray.length; i++) {
    optionArray[i] = optionArray[i].filter(onlyUnique);
    optionArray[i].sort();
  }

  // console.log("updated", optionArray);

  for (i = 0; i < dropDownArray.length; i++) {
    selectList = dropDownArray[i];
    optionList = optionArray[i];

    if (!$reset) {
      if (i != $index) {
        j = selectList.options.length;
        while (j--) { if (j > 1) selectList.options[j] = null; }
        // console.log($index, selectList, selectList.options);

        for (j = 0; j < optionList.length; j++) {
          var option = document.createElement("option");
          option.setAttribute("value", optionList[j]);
          option.text = optionList[j];
          selectList.appendChild(option);
        }
      }
    } else {
      j = selectList.options.length;
      while (j--) { if (j > 1) selectList.options[j] = null; }
      // console.log(selectList, selectList.options);

      for (j = 0; j < optionList.length; j++) {
        var option = document.createElement("option");
        option.setAttribute("value", optionList[j]);
        option.text = optionList[j];
        selectList.appendChild(option);
      }
    }
  }
}





function autoFilterData() {
  tempArray = sheetData.slice(0);

  // A - 0 - Country
  // B - 1 - Site
  // C - 2 - Publisher
  // E - 4 - Primary Context
  // G - 6 - Gender
  // I - 8 - Masthead
  // J - 9 - DMPU
  // L - 11 - Rise
  // M - 12 - Skins
  // N - 13 - Mini/Interscroller

  // console.log(queryArray.lengthqueryArray, tempArray);

  for (i = 0; i < queryArray.length; i++) {
    var q = tempArray.length;
    // console.log(q, i, tempArray);
    switch (i) {
      case 0:
        console.log("ASD", queryArray[i], tempArray);
        while (q--) {
          if (queryArray[i] != "") {
            // console.log(tempArray[q][0]);
            // if (tempArray[q][0].trim() === "TR") console.log(q, "found tr");
            // if (tempArray[q][0].trim() === "TR") {
              // console.log(q, "found tr");
            if (queryArray[i] == "TR") {
              // console.log("searching for tr");
              if (tempArray[q][0].trim() != "TR") tempArray.splice(q, 1);
            } else {
              if (tempArray[q][0] != queryArray[i]) tempArray.splice(q, 1);
            }
          }
        }
        break;
      case 1:
        while (q--) { if (queryArray[i] != "") { if (tempArray[q][1] != queryArray[i]) tempArray.splice(q, 1); } }
        break;
      case 2:
        while (q--) { if (queryArray[i] != "") { if (tempArray[q][2] != queryArray[i]) tempArray.splice(q, 1); } }
        break;
      case 3:
        while (q--) { if (queryArray[i] != "") { if (tempArray[q][4] != queryArray[i]) tempArray.splice(q, 1); } }
        break;
      case 4:
        while (q--) { if (queryArray[i] != "") { if (tempArray[q][6] != queryArray[i]) tempArray.splice(q, 1); } }
        break;
      case 5:
        while (q--) { if (queryArray[i] != "") { if (tempArray[q][8] != queryArray[i]) tempArray.splice(q, 1); } }
        break;
      case 6:
        while (q--) { if (queryArray[i] != "") { if (tempArray[q][9] != queryArray[i]) tempArray.splice(q, 1); } }
        break;
      case 7:
        while (q--) { if (queryArray[i] != "") { if (tempArray[q][11] != queryArray[i]) tempArray.splice(q, 1); } }
        break;
      case 8:
        while (q--) { if (queryArray[i] != "") { if (tempArray[q][12] != queryArray[i]) tempArray.splice(q, 1); } }
        break;
      case 9:
        while (q--) { if (queryArray[i] != "") { if (tempArray[q][13] != queryArray[i]) tempArray.splice(q, 1); } }
        break;
    }
  }

  console.log(":: filtered results - ", tempArray);

  // var previewArray = [], content = "";
  // for (i = 0; i < tempArray.length; i++) previewArray.push(tempArray[i][1]);
  // previewArray = previewArray.filter(onlyUnique);
  // for (i = 0; i < previewArray.length; i++) content += "<div><p>" + previewArray[i] + "</p></div>";
  // preview.innerHTML = content;

  var previewArray = [], content = "";
  for (i = 0; i < tempArray.length; i++) previewArray.push([tempArray[i][1], tempArray[i][19]]);
  previewArray = multiDimensionalUnique(previewArray);

  for (i = 0; i < previewArray.length; i++) {
    if (previewArray[i][1].toUpperCase().trim() === "Y") {
      content += "<div><p style='color:#ed1c56;font-weight:700'>" + previewArray[i][0].toLowerCase() + "</p></div>";
    } else {
      content += "<div><p>" + previewArray[i][0].toLowerCase() + "</p></div>";
    }
  }
  preview.innerHTML = content;

  (tempArray.length === 1) ? document.getElementById("previewText").innerHTML = "Filtered out " + previewArray.length + " site out of " + sheetData.length : document.getElementById("previewText").innerHTML = "Filtered out " + previewArray.length + " sites out of " + sheetData.length;
}





function openData() {
  var w = window.open("", "Custom Site List", "width=400,height=400,resizeable,scrollbars"), content = "<p>", temp = [];

  // for (i = 0; i < tempArray.length; i++) temp[i] = tempArray[i][1];
  // for (i = 0; i < customArray.length; i++) temp[i] = customArray[i];
  for (i = 0; i < customArray.length; i++) temp[i] = customArray[i][0];
  temp.sort();

  // for (i = 0; i < tempArray.length; i++) content += temp[i] + "<br/>";
  for (i = 0; i < customArray.length; i++) content += temp[i] + "<br/>";
  content += "</p>";
  w.document.title = "TI Media Publishing - Custom Site List";
  w.document.body.innerHTML = content;
}





// function addData() {
//   i = tempArray.length;
//   while (i--) customArray.unshift(tempArray[i][1]);
//   // for (i = 0; i < tempArray.length; i++) customArray.push(tempArray[i][1]);
//
//   customArray = customArray.filter(onlyUnique);
//   // customArray.sort();
//
//
//   var content = "";
//   for (i = 0; i < customArray.length; i++) content += "<div style='width:100%'><div id='deleteButton" + i + "' class='deleteButton'></div><p>" + customArray[i] + "</p></div>";
//
//   resetDropDowns();
//
//   var x = document.getElementsByClassName("deleteButton");
//   for (i = 0; i < x.length; i++) {
//     x[i].addEventListener("click", deleteButtonHandler);
//     x[i].style.cursor = "pointer";
//   }
//
//   preview.innerHTML = "";
//   output.innerHTML = content;
//   previewButtons.style.display = "none";
//   outputButtons.style.display = "block";
//
//   document.getElementById("previewText").innerHTML = "Filtered sites";
//   (customArray.length === 1) ? document.getElementById("resultsText").innerHTML = customArray.length + " site in your custom site list" : document.getElementById("resultsText").innerHTML = customArray.length + "  sites in your custom site list";
//
//   window.scrollTo(0, document.body.scrollHeight);
// }
function addData() {
  i = tempArray.length;
  while (i--) customArray.unshift([tempArray[i][1], tempArray[i][19]]);
  // for (i = 0; i < tempArray.length; i++) customArray.push(tempArray[i][1]);

  // customArray = customArray.filter(onlyUnique);
  customArray = multiDimensionalUnique(customArray);
  // customArray.sort();


  var content = "";
  // for (i = 0; i < customArray.length; i++) content += "<div style='width:100%'><div id='deleteButton" + i + "' class='deleteButton'></div><p>" + customArray[i][0] + "</p></div>";

  for (i = 0; i < customArray.length; i++) {
    if (customArray[i][1].toUpperCase().trim() === "Y") {
      content += "<div style='width:100%'><div id='deleteButton" + i + "' class='deleteButton'><script>addEventListener('click', deleteButtonHandler);</script></div><p style='color:#ed1c56;font-weight:700;'>" + customArray[i][0].toLowerCase() + "</p></div>";
    } else {
      content += "<div><div id='deleteButton" + i + "' class='deleteButton'><script>addEventListener('click', deleteButtonHandler)</script></div><p>" + customArray[i][0].toLowerCase() + "</p></div>";
    }
  }
  // preview.innerHTML = content

  output.innerHTML = content;



  resetDropDowns();


  var x = document.getElementsByClassName("deleteButton");
  for (i = 0; i < x.length; i++) {
    x[i].addEventListener("click", deleteButtonHandler);
    x[i].style.cursor = "pointer";
  }





  preview.innerHTML = "";
  previewButtons.style.display = "none";
  outputButtons.style.display = "block";

  document.getElementById("previewText").innerHTML = "Filtered sites";
  (customArray.length === 1) ? document.getElementById("resultsText").innerHTML = customArray.length + " site in your custom site list" : document.getElementById("resultsText").innerHTML = customArray.length + "  sites in your custom site list";

  window.scrollTo(0, document.body.scrollHeight);
}



function updateOutput() {
  var content = "";
  // for (i = 0; i < customArray.length; i++) content += "<div><div id='deleteButton" + i + "' class='deleteButton'><script>addEventListener('click', deleteButtonHandler)</script></div><p>" + customArray[i] + "</p></div>";
  for (i = 0; i < customArray.length; i++) {
    if (customArray[i][1].toUpperCase().trim() === "Y") {
      content += "<div style='width:100%'><div id='deleteButton" + i + "' class='deleteButton'><script>addEventListener('click', deleteButtonHandler);</script></div><p style='color:#ed1c56;font-weight:700;'>" + customArray[i][0].toLowerCase() + "</p></div>";
    } else {
      content += "<div><div id='deleteButton" + i + "' class='deleteButton'><script>addEventListener('click', deleteButtonHandler)</script></div><p>" + customArray[i][0].toLowerCase() + "</p></div>";
    }
  }
  output.innerHTML = content;


  // console.log("custom array", customArray);

  resetDropDowns();

  var x = document.getElementsByClassName("deleteButton");
  for (i = 0; i < x.length; i++) {
    x[i].addEventListener("click", deleteButtonHandler);
    x[i].style.cursor = "pointer";
  }

  outputButtons.style.display = "block";

  (customArray.length === 1) ? document.getElementById("resultsText").innerHTML = customArray.length + " site in your custom site list" : document.getElementById("resultsText").innerHTML = customArray.length + "  sites in your custom site list";
}

function clearData() {
  customArray = [];

  resetDropDowns();
  output.innerHTML = "";

  dropdownButtons.style.display = "none";
  outputButtons.style.display = "none";

  document.getElementById("previewText").innerHTML = "Filtered sites";
  document.getElementById("resultsText").innerHTML = "Custom site list";
}

function saveData() {
  // A - 0 - Country
  // B - 1 - Site
  // C - 2 - Publisher
  // E - 4 - Primary Context
  // G - 6 - Gender
  // I - 8 - Masthead
  // J - 9 - DMPU
  // L - 11 - Rise
  // M - 12 - Skins
  // N - 13 - Mini/Interscroller

  var a = [], b, c, d;
  // var a = [], b, c, d;
  //
  // for (i = 0; i < tempArray.length; i++) {
  //   var t = [];
  //   t.push({value:tempArray[i][0], type:"string"});
  //   t.push({value:tempArray[i][1], type:"string"});
  //   t.push({value:tempArray[i][2], type:"string"});
  //   t.push({value:tempArray[i][4], type:"string"});
  //   t.push({value:tempArray[i][6], type:"string"});
  //   t.push({value:tempArray[i][8], type:"string"});
  //   t.push({value:tempArray[i][9], type:"string"});
  //   t.push({value:tempArray[i][11], type:"string"});
  //   t.push({value:tempArray[i][12], type:"string"});
  //   t.push({value:tempArray[i][13], type:"string"});
  //   a[i] = t;
  // }
  //
  // b = [
  //   {value:"Country", type:"string"},
  //   {value:"Site", type:"string"},
  //   {value:"Publisher", type:"string"},
  //   {value:"Primary Context", type:"string"},
  //   {value:"Gender", type:"string"},
  //   {value:"Masthead", type:"string"},
  //   {value:"DMPU", type:"string"},
  //   {value:"Rise", type:"string"},
  //   {value:"Skins", type:"string"},
  //   {value:"Mini/Interscroller", type:"string"}
  // ];
  // a.unshift(b);

  b = customArray.slice(0);
  b.sort();

  for (i = 0; i < b.length; i++) {
    var t = [];
    // t.push({value:b[i], type:"string"});
    t.push({value:b[i][0], type:"string"});
    a[i] = t;
  }

  // console.log(a);

  d = new Date();
  c = "custom-site-list-" + new Date().getTime();

  xlsx = {
    filename: c,
    sheet: {
	     data: a
    }
  }

  zipcelx(xlsx);
}





function resetDropDowns() {
  var dropDownArray = [
    document.getElementById("countryDropdown"), document.getElementById("siteDropdown"), document.getElementById("publisherDropdown"),
    document.getElementById("primaryContextDropdown"), document.getElementById("genderDropdown"), document.getElementById("mastheadDropdown"),
    document.getElementById("dmpuDropdown"), document.getElementById("riseDropdown"), document.getElementById("skinsDropdown"),
    document.getElementById("miniInterscrollerDropdown")
  ];

  for (i = 0; i < dropDownArray.length; i++) dropDownArray[i].selectedIndex = 0;

  tempArray = [];
  tempArray = sheetData.slice(0);
  console.log(":: new data array created ::");
  // console.log(":: tempArray ::", tempArray);

  queryArray = ["", "", "", "", "", "", "", "", "", ""];

  updateDropdowns(true);

  dropdownButtons.style.display = "none";
  previewButtons.style.display = "none";

  document.getElementById("previewText").innerHTML = "Filtered sites";
  preview.innerHTML = "";
}





function dropDownHandler($e) {
  dropdown = $e.currentTarget;
  type = $e.type;

  tempArray = sheetData.slice(0);

  dropdownButtons.style.display = "block";
  previewButtons.style.display = "block";

  // A - 0 - Country
  // B - 1 - Site
  // C - 2 - Publisher
  // E - 4 - Primary Context
  // G - 6 - Gender
  // I - 8 - Masthead
  // J - 9 - DMPU
  // L - 11 - Rise
  // M - 12 - Skins
  // N - 13 - Mini/Interscroller

  switch (dropdown) {
    case document.getElementById("countryDropdown"): queryArray[0] = dropdown.options[dropdown.selectedIndex].value; break;
    case document.getElementById("siteDropdown"): queryArray[1] = dropdown.options[dropdown.selectedIndex].value; break;
    case document.getElementById("publisherDropdown"): queryArray[2] = dropdown.options[dropdown.selectedIndex].value; break;
    case document.getElementById("primaryContextDropdown"): queryArray[3] = dropdown.options[dropdown.selectedIndex].value; break;
    case document.getElementById("genderDropdown"): queryArray[4] = dropdown.options[dropdown.selectedIndex].value; break;
    case document.getElementById("mastheadDropdown"): queryArray[5] = dropdown.options[dropdown.selectedIndex].value; break;
    case document.getElementById("dmpuDropdown"): queryArray[6] = dropdown.options[dropdown.selectedIndex].value; break;
    case document.getElementById("riseDropdown"): queryArray[7] = dropdown.options[dropdown.selectedIndex].value; break;
    case document.getElementById("skinsDropdown"): queryArray[8] = dropdown.options[dropdown.selectedIndex].value; break;
    case document.getElementById("miniInterscrollerDropdown"): queryArray[9] = dropdown.options[dropdown.selectedIndex].value; break;
  }

  autoFilterData();

  switch (dropdown) {
    case document.getElementById("countryDropdown"): updateDropdowns(false, 0); break;
    case document.getElementById("siteDropdown"): updateDropdowns(false, 1); break;
    case document.getElementById("publisherDropdown"): updateDropdowns(false, 2); break;
    case document.getElementById("primaryContextDropdown"): updateDropdowns(false, 3); break;
    case document.getElementById("genderDropdown"): updateDropdowns(false, 4); break;
    case document.getElementById("mastheadDropdown"): updateDropdowns(false, 5); break;
    case document.getElementById("dmpuDropdown"): updateDropdowns(false, 6); break;
    case document.getElementById("riseDropdown"): updateDropdowns(false, 7); break;
    case document.getElementById("skinsDropdown"): updateDropdowns(false, 8); break;
    case document.getElementById("miniInterscrollerDropdown"): updateDropdowns(false, 9); break;
  }
}





function buttonHandler($e) {
  button = $e.currentTarget;
  type = $e.type;

  switch (button) {
    case document.getElementById("addButton"):
      addData();
      break;

    case document.getElementById("clearButton"):
      clearData();
      break;

    case selectButton:
      selectText("output");
      document.getElementById("selectButton").style.display = "none";
      document.getElementById("copyButton").style.display = "block";
      break;

    case copyButton:
      document.execCommand("copy");
      document.getElementById("copyButton").innerHTML = "Custom list copied!";
      setTimeout(function() {
        document.getElementById("selectButton").style.display = "block";
        document.getElementById("copyButton").style.display = "none";
        document.getElementById("copyButton").innerHTML = "Copy custom list";
      }, 2000);
      break;

    case document.getElementById("viewButton"):
      openData();
      break;

    case document.getElementById("resetButton"):
      console.log(":: cleared and reset data ::");
      resetDropDowns();
      break;

    case document.getElementById("downloadButton"):
      saveData();
      break;
  }
}





function deleteButtonHandler($e) {
  button = $e.currentTarget;
  // console.log(button);

  var index = parseInt(button.id.substring(button.id.indexOf("deleteButton") + 12, button.id.length));

  customArray.splice(index, 1);
  console.log(customArray);

  updateOutput();


  if (customArray.length == 0) clearData();
}





function onlyUnique($value, $index, $self) { return $self.indexOf($value) === $index; }

function multiDimensionalUnique($arr) {
  var uniques = [];
  var itemsFound = {};
  for(var i = 0, l = $arr.length; i < l; i++) {
      var stringified = JSON.stringify($arr[i]);
      if(itemsFound[stringified]) { continue; }
      uniques.push($arr[i]);
      itemsFound[stringified] = true;
  }
  return uniques;
}

function fixString($string) {
  if ($string.substring($string.length - 1, $string.length) === "/") $string = $string.substring(0, $string.length - 1);
  $string = $string.toLowerCase();
  $string = $string.trim();
  return $string.charAt(0).toUpperCase() + $string.slice(1);
}

function titleCase($string) {
  return $string.replace(/\w\S*/g, function($txt) {
    return $txt.charAt(0).toUpperCase() + $txt.substr(1).toLowerCase();
  });
}

function selectText(id) {
  if (window.getSelection) {
    if (window.getSelection().empty) {  // Chrome
      window.getSelection().empty();
    } else if (window.getSelection().removeAllRanges) {  // Firefox
      window.getSelection().removeAllRanges();
    }
  } else if (document.selection) {  // IE?
    document.selection.empty();
  }

	var sel, range;
	var el = document.getElementById(id); //get element id
	if (window.getSelection && document.createRange) { //Browser compatibility
    sel = window.getSelection();
    if (sel.toString() == '') { //no text selection
		  window.setTimeout(function() {
  			range = document.createRange(); //range object
  			range.selectNodeContents(el); //sets Range
  			sel.removeAllRanges(); //remove all ranges from selection
  			sel.addRange(range);//add Range to a Selection.
		  }, 1);
	  }
	} else if (document.selection) { //older ie
		sel = document.selection.createRange();
		if (sel.text == '') { //no text selection
			range = document.body.createTextRange();//Creates TextRange object
			range.moveToElementText(el);//sets Range
			range.select(); //make selection.
		}
	}
}





domready(function() {
	console.log(":: dom ready ::");
  if (divScraper) init();
});
